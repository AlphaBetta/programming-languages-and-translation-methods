/**
 * Транслятор вывода команды cal в столбцовый вариант.
 *
 * Copyright (c) 2020, Ruslan Sergeev <ruslansergeevab@gmail.com>
 *
 * This code is licensed under a MIT-style license.
 * 
 */

%{
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdint.h>
#define N_DAYS_IN_WEEK 7
#define DAY_NAME_LENGTH 3

int yydebug = 1;

typedef struct _node_day {
        uint8_t day;                    // Число  
        struct _node_day* last;         // Указатель на последний элемент в списке
        struct _node_day* previous;     // Указатель на предыдущий элемент в списке
} node_day;

typedef struct _node_week {
        node_day* first_day;            // Указатель на элемент СПИСКА ДНЕЙ текущей недели
        uint8_t num_days;               // Количество дней в неделе
        uint8_t num_week;               // Порядковый номер недели
        struct _node_week* next;        // Указатель на следующую неделю
} node_week;

typedef struct _struct_calendar {
        char** days_of_week;            // Массив для хранения названий дней недели
        node_week* first_week;          // Указатель на первый элемент в списке недель
        node_week* last_week;           // Указатель на последнюю неделю
        uint8_t n_weeks;                // Количество недель в месяце
} struct_calendar;


/*
 * Функция извлекает последний день из списка недель и удаляет его из этого списка.
 * Также изменяется значение поля num_days.
 */
uint8_t pop_day(node_week* week);

struct_calendar* calendar;	            // Переменная для хранения структуры календаря
uint8_t cur_day_of_week;                // Счётчкик названий дней недели

void yyerror(const char *str)
{
        fprintf(stderr,"error: %s\n", str);
}
 
int yywrap()
{
        return 1;
}
  
int main()
{
        calendar = (struct_calendar*) malloc(sizeof(struct_calendar));                  // Переменная для хранения структуры календаря
        calendar->days_of_week = (char**) malloc(sizeof(char*) * N_DAYS_IN_WEEK);       // Выделяем память для хранения названий дней недели
        calendar->n_weeks = 0;                                                          // Сначала количество недель в месяце = 0

        cur_day_of_week = 6;                                                            // Счётчкик названий дней недели. Нулевой день - Su или Mo 
	
        yyparse();
}

/*
 * Функция извлекает последний день из списка недель и удаляет его из этого списка.
 * Также изменяется значение поля num_days.
 */
uint8_t pop_day(node_week* week)
{
    uint8_t result;
    
    node_day* last_day = week->first_day->last;     // Сохраняем указатель на последний день для возможности освободить память.
     
    result = last_day->day;
    week->first_day->last = last_day->previous;
    week->num_days--;                               // Уменьшаем количество дней в неделе
    
    free(last_day);                                 // Освобождаем память
    
    return result; 
}

%}


%union 
{
        uint32_t number;
        char* string;
}
        
%token <number> NUMBER

%token <string> MONTH DAY

%token <string> NL

%%
calendar:       month_year days_of_week weeks NL
        {
                uint8_t days_to_process = N_DAYS_IN_WEEK;       // Количество дней, которые ещё нужно обработать
                node_week* cur_week = calendar->first_week;
                for (uint8_t i = 0; i < N_DAYS_IN_WEEK; i++) {
                    printf("%s ", calendar->days_of_week[i]);
                    /* Обрабатываем по каждому дню из каждой недели */
                    while (cur_week != NULL) {
                        /* Работаем с первой неделей */
                        if (cur_week->num_week == 1) {
                            if (cur_week->num_days < days_to_process) {
                                printf("   ");       // Три пробела
                            } else {
                                printf("%2hu ", pop_day(cur_week));      // Вывод числа
                            }
                            cur_week = cur_week->next;
                            continue;
                        }
                        /* Работаем с последней неделей */
                        if (cur_week->num_week == calendar->last_week->num_week) {
                            if (cur_week->num_days > 0) {
                                printf("%2hu ", pop_day(cur_week));      // Вывод числа
                            } else {
                                printf("   ");       // Три пробела
                            }
                            printf("\n");
                            cur_week = cur_week->next;
                            continue;
                        }
                        /* Работаем не с последней и не с первой неделями */
                        printf("%2hu", pop_day(cur_week));
                        printf(" ");        // Один пробел между числами
                        cur_week = cur_week->next;
                    }
                    days_to_process--;
                    cur_week = calendar->first_week;            
                }
                
        }
        ;

month_year:     MONTH NUMBER NL
        {       
                /* Просто выводим распознанные лексемы */
                printf("    %s ", $1);
                printf("%d\n", $2);
        }
        ;
        
days_of_week:   DAY days_of_week
        {
                /* 
                 * Ниже обрабатывыется последнее название дня недели.
                 * Здесь обрабатываются все остальные названия дней недели.
                 * (Так как рекурсия уходит вглубь до последнего элемента)
                 */
                calendar->days_of_week[cur_day_of_week] = (char*) malloc(sizeof(char) * DAY_NAME_LENGTH);
                strcpy(calendar->days_of_week[cur_day_of_week], $1);
                cur_day_of_week--;
                
        }
                |
                DAY NL
        {
                /* Здесь обрабатывается самое последнее название дня недели (пояснение см. выше)  */    
                calendar->days_of_week[cur_day_of_week] = (char*) malloc(sizeof(char) * DAY_NAME_LENGTH);
                strcpy(calendar->days_of_week[cur_day_of_week], $1);
                cur_day_of_week--;
                
        }
        ;
        
weeks:          week weeks
                |
                week
        ;
        
week:           NUMBER week
        {
                /* Создаём новый элемент списка дней */
                node_day* new_day_in_week = (node_day*) malloc(sizeof(node_day));
                new_day_in_week->day = $1;
                new_day_in_week->last = new_day_in_week;
                new_day_in_week->previous = calendar->last_week->first_day->last;
                
                calendar->last_week->num_days++;                                 // Увеличиваем количество дней в неделе         
                calendar->last_week->first_day->last = new_day_in_week;          // Теперь последний элемент списка дней = new_day_in_week  
                
        }
                |
                NUMBER NL
        {
                node_week* new_week = (node_week*) malloc(sizeof(node_week));
                
                /* Добавляем день в неделю */
                new_week->first_day = (node_day*) malloc(sizeof(node_day));
                new_week->first_day->day = $1;
                new_week->num_days = 1;
                new_week->first_day->last = new_week->first_day;                // Первый день в неделе пока является последним
                new_week->first_day->previous = NULL;                           // У первого дня в неделее нет предыдущего
                new_week->next = NULL;                                          // У новой недели указатель на следующий элемент неопределён
                
                calendar->n_weeks++;                                            // Увеличиваем число недель
                new_week->num_week = calendar->n_weeks;                         // Записываем номер добавляемой недели 
                
                /* Необходимо добавить новую неделю в список недель */
                if (calendar->n_weeks > 1) {
                    calendar->last_week->next = new_week;                       // Указатель последней недели в списке до добавления новой недели = new_week
                    calendar->last_week = new_week;                             // Указатель на последнюю неделю  = добавленная неделя
                } else {
                    calendar->first_week = new_week;
                    calendar->last_week = new_week;
                }
                                             
        }           
        
        ;

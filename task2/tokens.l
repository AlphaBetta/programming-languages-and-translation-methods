/**
 * Лексемы для транслятора вывода команды cal в столбцовый вариант.
 *
 * Copyright (c) 2020, Ruslan Sergeev <ruslansergeevab@gmail.com>
 *
 * This code is licensed under a MIT-style license.
 * 
 */

%{
#include <stdio.h>
#include <string.h>
#include "y.tab.h"
%}
%%
[0-9]+                                                                                          yylval.number = atoi(yytext); return NUMBER;
December|January|February|March|April|May|June|July|August|September|October|November           yylval.string = strdup(yytext); return MONTH;
Mo|Tu|We|Th|Fr|Sa|Su										yylval.string = strdup(yytext); return DAY;
\n                                                                                              return NL;      /* Символ перевода строки */
[ \t]+                                                                                                          /* Игнорируем пробелы */;
%%
